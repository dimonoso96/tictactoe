﻿using System.Collections.Generic;
using UnityEngine;
using PanelManagement.Data;
using PanelManagement.Panel;

namespace PanelManagement
{
    public interface IPanelManager
    {
        void AddPanel(string key, PanelElement element);

        void ShowPanel(string panelKey, AbstractPanelData data = null);

        void HidePanel(string panelKey);
    }
}