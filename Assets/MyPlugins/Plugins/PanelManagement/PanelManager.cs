﻿using System.Collections.Generic;
using UnityEngine;
using PanelManagement.Data;
using PanelManagement.Panel;

namespace PanelManagement
{
    public class PanelManager : MonoBehaviour, IPanelManager
    {
        [SerializeField]
        private GameObject _mask;

        private readonly Dictionary<string, PanelElement> _panels = new Dictionary<string, PanelElement>();
        private readonly Dictionary<string, PanelElement> _activePanels = new Dictionary<string, PanelElement>();

        private void Start()
        {
            _mask.SetActive(false);
        }

        public void AddPanel(string key, PanelElement element)
        {
            _panels.Add(key, element);
        }

        public void ShowPanel(string panelKey, AbstractPanelData data = null)
        {
            _mask.SetActive(true);
            if (_activePanels.ContainsKey(panelKey) || !_panels.ContainsKey(panelKey))
            {
                return;
            }
            var panel = _panels[panelKey];
            panel.Show(data);
            _activePanels.Add(panelKey, panel);
        }

        public void HidePanel(string panelKey)
        {
            if (!_activePanels.ContainsKey(panelKey))
            {
                return;
            }

            var panel = _panels[panelKey];
            panel.Hide();
            _activePanels.Remove(panelKey);
            if (_activePanels.Count == 0)
            {
                _mask.SetActive(false);
            }
        }
    }
}