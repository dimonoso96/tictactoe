﻿using PanelManagement.Data;
using UnityEngine;
using Zenject;

namespace PanelManagement.Panel
{
    public class PanelElement : MonoBehaviour
    {
        [SerializeField]
        protected string PanelName;

        [Inject]
        protected IPanelManager PanelManager;

        protected virtual void Awake()
        {
            PanelManager.AddPanel(PanelName, this);
            gameObject.SetActive(false);
        }

        public virtual void Show(AbstractPanelData data)
        {
            gameObject.SetActive(true);
        }

        public virtual void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}
