﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StateMachine
{
    // if (!typeof(T).IsEnum) throw new ArgumentException("T must be an enumerated type");
    public interface IState<T> where T : struct, IComparable, IFormattable, IConvertible
    {
        event Action OnEnterState;
        event Action OnExitState;
        event Action OnExecute;

        void EnterState();
        void ExitState();
        void Execute();
    }
}
