﻿using System;
using System.Collections.Generic;

namespace StateMachine
{
    public interface IStateMachine<T> where T : struct, IComparable, IFormattable, IConvertible
    {
        T? InitState { get; set; }
        T? CurrentState { get; }

        Dictionary<T, IState<T>> States { get; }
        Dictionary<T, List<T>> TriggersList { get; }

        void Init();

        void Execute();

        void AddTrigger(T fromState, T toState);

        void ChangeStateOnTrigger(T state);

        void AddState(T type, IState<T> state);
    }
}
