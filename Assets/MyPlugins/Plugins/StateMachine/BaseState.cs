﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StateMachine
{
    public class BaseState<T> : IState<T> where T : struct, IComparable, IFormattable, IConvertible
    {
        public event Action OnEnterState;
        public event Action OnExitState;
        public event Action OnExecute;

        public void EnterState()
        {
            if (OnEnterState != null)
            {
                OnEnterState();
            }
        }

        public void ExitState()
        {
            if (OnExitState != null)
            {
                OnExitState();
            }
        }
        
        public void Execute()
        {
            if (OnExecute != null)
            {
                OnExecute();
            }
        }
    }
}
