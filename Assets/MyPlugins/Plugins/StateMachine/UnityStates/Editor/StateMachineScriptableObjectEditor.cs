﻿using System;
using StateMachine.ScriptableObjects;
using UnityEditor;
using UnityEngine;

namespace StateMachine
{
    //[CustomEditor(typeof(StateMachineScriptableObject<>))] 
    public abstract class StateMachineScriptableObjectEditor<T> : Editor where T : struct, IComparable, IFormattable, IConvertible
    {
        static bool showStatesEditor = false;
        static bool showTriggersEditor = false;

        StateMachineScriptableObject<T> _serializedObject;

        public void OnEnable()
        {
            _serializedObject = (StateMachineScriptableObject<T>)target;
        }

        public override void OnInspectorGUI()
        {
            if (!typeof(T).IsEnum)
            {
                Debug.LogError("T must be an enumerated type");
                return;
            }

            _serializedObject.InitialState = (T)(object)(EditorGUILayout.EnumPopup("InitialState", (Enum)(object)_serializedObject.InitialState));

            var styleMarginLeft20 = new GUIStyle();
            styleMarginLeft20.margin.left = 20;

            var styleMarginLeft10 = new GUIStyle();
            styleMarginLeft10.margin.left = 10;

            //=================================================================================================================================================
            showStatesEditor = EditorGUILayout.Foldout(showStatesEditor, "States");

            if (showStatesEditor)
            {
                EditorGUILayout.BeginVertical(styleMarginLeft20);
                int length = 0;
                if (_serializedObject.States != null)
                {
                    length = _serializedObject.States.Length;
                }

                length = EditorGUILayout.IntField("Size", length);

                if (length != 0 && (_serializedObject.States == null || length != _serializedObject.States.Length))
                {
                    var temp = new T[length];
                    if (_serializedObject.States != null)
                    {
                        for (int i = 0; i < _serializedObject.States.Length && i < length; i++)
                        {
                            temp[i] = _serializedObject.States[i];
                        }
                    }
                    _serializedObject.States = temp;
                }

                for (int i = 0; i < length; i++)
                {
                    _serializedObject.States[i] = (T)(object)(EditorGUILayout.EnumPopup("Element " + i.ToString(), (Enum)(object)_serializedObject.States[i]));
                }
                EditorGUILayout.EndVertical();
            }

            //=================================================================================================================================================
            // TODO: serialize triggers
            showTriggersEditor = EditorGUILayout.Foldout(showTriggersEditor, "Triggers");

            if (showTriggersEditor)
            {
                EditorGUILayout.BeginVertical(styleMarginLeft20);

                int length = 0;

                if (_serializedObject.fromList != null && _serializedObject.toList != null)
                {
                    length = Math.Min(_serializedObject.fromList.Length, _serializedObject.toList.Length);
                }

                length = EditorGUILayout.IntField("Size", length);

                if (length != 0 && (_serializedObject.fromList == null || _serializedObject.toList == null || length != _serializedObject.fromList.Length || length != _serializedObject.toList.Length))
                {
                    var tempFrom = new T[length];
                    var tempTo = new T[length];
                    if (_serializedObject.fromList != null && _serializedObject.toList != null)
                    {
                        for (int i = 0; i < _serializedObject.fromList.Length && i < _serializedObject.toList.Length && i < length; i++)
                        {
                            tempFrom[i] = _serializedObject.fromList[i];
                            tempTo[i] = _serializedObject.toList[i];
                        }
                    }
                    _serializedObject.fromList = tempFrom;
                    _serializedObject.toList = tempTo;
                }

                EditorGUILayout.BeginHorizontal(styleMarginLeft10);
                EditorGUILayout.LabelField("From");
                EditorGUILayout.LabelField("To");
                EditorGUILayout.EndHorizontal();

                for (int i = 0; i < length; i++)
                {
                    EditorGUILayout.BeginHorizontal(styleMarginLeft10);
                    _serializedObject.fromList[i] = (T)(object)(EditorGUILayout.EnumPopup((Enum)(object)_serializedObject.fromList[i]));
                    _serializedObject.toList[i] = (T)(object)(EditorGUILayout.EnumPopup((Enum)(object)_serializedObject.toList[i]));
                    EditorGUILayout.EndHorizontal();
                }
                EditorGUILayout.EndVertical();
            }

            EditorUtility.SetDirty(target);
        }
    }
}
