﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace StateMachine.ScriptableObjects
{
    public abstract class StateMachineScriptableObject<T> : ScriptableObject where T : struct, IComparable, IFormattable, IConvertible
    {
        public T InitialState;

        public T[] States;

        public T[] fromList;

        public T[] toList;
    }
}
