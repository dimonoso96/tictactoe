﻿using System;
using System.Collections.Generic;
using Zenject;

namespace StateMachine
{
    public class BaseStateMachine<T> : ITickable, IStateMachine<T> where T : struct, IComparable, IFormattable, IConvertible
    {
        private T? _initState;
        public T? InitState { get { return _initState; } set { if (!_initState.HasValue) { _initState = value; } } }

        private T? _currentState;
        public T? CurrentState { get { return _currentState; } private set { _currentState = value; } }

        protected Dictionary<T, IState<T>> _states = new Dictionary<T, IState<T>>();
        public Dictionary<T, IState<T>> States { get { return _states; } }

        private Dictionary<T, List<T>> _triggersList = new Dictionary<T, List<T>>();
        public Dictionary<T, List<T>> TriggersList
        {
            get
            {
                return _triggersList;
            }
            private set
            {
                _triggersList = value;
            }
        }

        public void AddTrigger(T fromState, T toState)
        {
            if (!TriggersList.ContainsKey(fromState))
            {
                var list = new List<T>();
                TriggersList.Add(fromState, list);
            }
            TriggersList[fromState].Add(toState);
        }

        public void Init()
        {
            if (InitState.HasValue)
            {
                ChangeState(InitState.Value);
            }
        }

        public void ChangeStateOnTrigger(T state)
        {
            if (CurrentState.HasValue)
            {
                var statesList = TriggersList[CurrentState.Value];
                for (int i = 0; i < statesList.Count; i++)
                {
                    if (state.Equals(statesList[i]))
                    {
                        ChangeState(state);
                        return;
                    }
                }
            }
        }

        public void AddState(T type, IState<T> state)
        {
            _states.Add(type, state);
        }

        private void ChangeState(T type)
        {
            if (CurrentState.HasValue)
            {
                var oldState = States[CurrentState.Value];
                oldState.ExitState();
            }
            CurrentState = type;
            var newState = States[CurrentState.Value];
            newState.EnterState();
        }

        public void Tick()
        {
            Execute();
        }

        public void Execute()
        {
            if (CurrentState.HasValue)
            {
                States[CurrentState.Value].Execute();
            }
        }
    }
}