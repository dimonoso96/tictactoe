﻿using System.Collections.Generic;
using UnityEngine;
using ScreenManagement.Screen;
using ScreenManagement.Data;

namespace ScreenManagement
{
    public interface IScreenManager
    {

        void AddScreen(string key, ScreenElement element);

        void ShowScreen(string screenKey, AbstractScreenData data = null);
    }
}