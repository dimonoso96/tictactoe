﻿using ScreenManagement.Data;
using UnityEngine;
using Zenject;

namespace ScreenManagement.Screen
{
    public class ScreenElement : MonoBehaviour
    {
        [SerializeField]
        protected string _screenName;

        [Inject]
        private IScreenManager _screenManager;

        protected virtual void Awake()
        {
            _screenManager.AddScreen(_screenName, this);
            gameObject.SetActive(false);
        }

        public virtual void Show(AbstractScreenData data)
        {
            gameObject.SetActive(true);
        }

        public virtual void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}