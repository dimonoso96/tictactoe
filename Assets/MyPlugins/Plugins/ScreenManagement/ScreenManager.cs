﻿using System.Collections.Generic;
using UnityEngine;
using ScreenManagement.Screen;
using ScreenManagement.Data;

namespace ScreenManagement
{
    public class ScreenManager : MonoBehaviour, IScreenManager
    {
        private Dictionary<string, ScreenElement> _huds = new Dictionary<string, ScreenElement>();
        private ScreenElement _activeScreen;

        public void AddScreen(string key, ScreenElement element)
        {
            _huds.Add(key, element);
        }

        public void ShowScreen(string screenKey, AbstractScreenData data = null)
        {
            if (!_huds.ContainsKey(screenKey))
            {
                return;
            }
            if (_activeScreen != null)
            {
                _activeScreen.Hide();
                _activeScreen = null;
            }
            _activeScreen = _huds[screenKey];
            _activeScreen.Show(data);
        }
    }
}