﻿using System;
using System.Collections.Generic;
using System.Linq;
using StateMachine;
using TicTacToe.Game.RuleChecking;
using TicTacToe.Game.State;
using TicTacToe.Player;
using TicTacToe.Player.Factory;
using TicTacToe.Player.State;
using UnityEngine;
using Zenject;

namespace TicTacToe.Game
{
    public class GameLogicManager : MonoBehaviour, IGameLogicManager
    {
        public event Action<IPlayer> OnPlayerWinEvent;
        public event Action<GameCharacter> OnCharacterWinEvent;
        public event Action<IPlayer> OnPlayerLoseEvent;
        public event Action OnDrawEvent;

        public const int ElementsWidth = 3;

        private Queue<IPlayer> _players = new Queue<IPlayer>();

        public IPlayer CurrentPlayer
        {
            get
            {
                if (_players.Count < 2)
                {
                    return null;
                }
                return _players.Peek();
            }
        }

        public TableCell[,] Table { get; private set; }

        [Inject]
        private RuleHelper _ruleHelper;

        [Inject]
        private IPlayerFactory _playerFactory;

        [Inject]
        private IStateMachine<GameStates> _stateMachine;
        public IStateMachine<GameStates> GameStateMachine
        {
            get
            {
                return _stateMachine;
            }
            private set
            {
                _stateMachine = value;
            }
        }

        private void Awake()
        {
            Table = new TableCell[ElementsWidth, ElementsWidth];
            for (int i = 0; i < ElementsWidth; i++)
            {
                for (int j = 0; j < ElementsWidth; j++)
                {
                    Table[i, j] = new TableCell();
                }
            }
            AddRules();
        }

        private void AddRules()
        {
            _ruleHelper.CreateRule(Table);
        }

        private void Start()
        {
            var inGameState = GameStateMachine.States[GameStates.InGame];
            inGameState.OnEnterState += StartLevel;
            inGameState.OnExitState += ClearPlayers;

            GameStateMachine.Init();
        }

        public void AddPlayer(PlayerType playerType)
        {
            if (GameStateMachine.CurrentState.HasValue)
            {
                switch (GameStateMachine.CurrentState.Value)
                {
                    case GameStates.InGame:
                        {
                            Debug.Log("Try to add player on incorrect state");
                            break;
                        }
                    case GameStates.WaitFirstPlayer:
                        {
                            var player = _playerFactory.Create(playerType);
                            player.Character = GameCharacter.X;
                            _players.Enqueue(player);
                            GameStateMachine.ChangeStateOnTrigger(GameStates.WaitSecondPlayer);
                            break;
                        }
                    case GameStates.WaitSecondPlayer:
                        {
                            var player = _playerFactory.Create(playerType);
                            player.Character = GameCharacter.O;
                            _players.Enqueue(player);
                            GameStateMachine.ChangeStateOnTrigger(GameStates.InGame);
                            break;
                        }
                }
            }
        }

        private void ClearPlayers()
        {
            if (_players.Count > 0)
            {
                _players.Peek().StateMachine.ChangeStateOnTrigger(PlayerState.WaitOtherPlayer);
            }
            _playerFactory.Clear();
            _players.Clear();
        }

        private void StartLevel()
        {
            ClearTable();
            _players.Peek().StateMachine.ChangeStateOnTrigger(PlayerState.Turn);
            Debug.Log("Start level");
        }

        public void AddCharacter(int x, int y, IPlayer player)
        {
            if (player != _players.Peek())
            {
                return;
            }
            if (Table[x, y].Character != GameCharacter.None)
            {
                return;
            }
            Table[x, y].Character = _players.Peek().Character;
            NextPlayer();
        }

        public void AddCharacter(TableCell cell, IPlayer player)
        {
            if (player != _players.Peek())
            {
                return;
            }
            if (cell.Character != GameCharacter.None)
            {
                return;
            }
            cell.Character = _players.Peek().Character;
            NextPlayer();
        }

        private void NextPlayer()
        {
            if (_players.Count > 0)
            {
                var winCharacter = _ruleHelper.GetWinCharacter();
                if(winCharacter != GameCharacter.None)
                {
                    CharacterWin(winCharacter);
                    return;
                }
                if(_ruleHelper.IsDraw())
                {
                    Draw();
                    return;
                }

                var oldPlayer = _players.Dequeue();
                oldPlayer.StateMachine.ChangeStateOnTrigger(PlayerState.WaitOtherPlayer);
                _players.Peek().StateMachine.ChangeStateOnTrigger(PlayerState.Turn);
                _players.Enqueue(oldPlayer);
            }
        }

        private void Draw()
        {
            Debug.Log("Draw");
            if (OnDrawEvent != null)
            {
                OnDrawEvent();
            }
            GameEnd();
        }

        private void CharacterWin(GameCharacter character)
        {
            Debug.Log("Win " + character.ToString());
            if (OnCharacterWinEvent != null)
            {
                OnCharacterWinEvent(character);
            }

            var playerWin = _players.First(x => x.Character == character);
            if (OnPlayerWinEvent != null)
            {
                OnPlayerWinEvent(playerWin);
            }

            var playerLose = _players.First(x => x.Character != character);
            if (OnPlayerLoseEvent != null)
            {
                OnPlayerLoseEvent(playerLose);
            }
            GameEnd();
        }

        private void ClearTable()
        {
            for (int i = 0; i < ElementsWidth; i++)
            {
                for (int j = 0; j < ElementsWidth; j++)
                {
                    Table[i, j].Character = GameCharacter.None;
                }
            }
        }

        private void GameEnd()
        {
            GameStateMachine.ChangeStateOnTrigger(GameStates.WaitFirstPlayer);
        }
    }
}
