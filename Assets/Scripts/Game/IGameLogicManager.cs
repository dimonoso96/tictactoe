﻿using System;
using StateMachine;
using TicTacToe.Game.State;
using TicTacToe.Player;

namespace TicTacToe.Game
{
    public interface IGameLogicManager
    {
        event Action<IPlayer> OnPlayerWinEvent;
        event Action<GameCharacter> OnCharacterWinEvent;
        event Action<IPlayer> OnPlayerLoseEvent;
        event Action OnDrawEvent;

        IPlayer CurrentPlayer { get; }

        TableCell[,] Table { get; }

        IStateMachine<GameStates> GameStateMachine { get; }

        void AddPlayer(PlayerType playerType);

        void AddCharacter(int x, int y, IPlayer player);
        void AddCharacter(TableCell cell, IPlayer player);
    }
}
