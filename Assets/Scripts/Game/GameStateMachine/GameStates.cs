﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TicTacToe.Game.State
{
    public enum GameStates
    {
        WaitFirstPlayer,
        WaitSecondPlayer,
        InGame,
    }
}
