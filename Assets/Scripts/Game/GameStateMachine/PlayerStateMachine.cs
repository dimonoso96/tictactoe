﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using StateMachine;
using StateMachine.ScriptableObjects;
using TicTacToe.Player.State;

namespace TicTacToe.Player.StateMachine
{
    [CreateAssetMenu(menuName = "StateMachines/PlayerStateMachine")]
    public class PlayerStateMachine : StateMachineScriptableObject<PlayerState>
    {
    }
}