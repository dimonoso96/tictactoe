﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using StateMachine;
using StateMachine.ScriptableObjects;
using TicTacToe.Game.State;

namespace TicTacToe.Game.StateMachine
{
    [CustomEditor(typeof(GameStateMachine))]
    public class GameStateMachineEditor : StateMachineScriptableObjectEditor<GameStates>
    {
	}
}
