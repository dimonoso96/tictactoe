﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using StateMachine;
using StateMachine.ScriptableObjects;
using TicTacToe.Player;
using TicTacToe.Player.State;

namespace TicTacToe.Player.StateMachine
{
    [CustomEditor(typeof(PlayerStateMachine))]
    public class PlayerStateMachineEditor : StateMachineScriptableObjectEditor<PlayerState>
    {
    }
}