﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using StateMachine;
using StateMachine.ScriptableObjects;
using TicTacToe.Game.State;

namespace TicTacToe.Game.StateMachine
{
    [CreateAssetMenu(menuName = "StateMachines/GameStateMachine")]
    public class GameStateMachine : StateMachineScriptableObject<GameStates>
    {
    }
}