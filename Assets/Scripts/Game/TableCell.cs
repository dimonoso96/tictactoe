﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TicTacToe.Player;

namespace TicTacToe.Game
{
    public class TableCell
    {
        public event System.Action<GameCharacter> OnValueChange;

        private GameCharacter _character = GameCharacter.None;
        public GameCharacter Character
        {
            get
            {
                return _character;
            }
            set
            {
                if (_character == value)
                {
                    return;
                }
                _character = value;
                if (OnValueChange != null)
                {
                    OnValueChange(_character);
                }
            }
        }
    }
}
