﻿using ScreenManagement;
using ScreenManagement.Screen;
using TicTacToe.Game;
using TicTacToe.Player;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace TicTacToe.GUI
{
    public class PlayersCreatorScreen : ScreenElement
    {
        [Inject]
        private IGameLogicManager _gameLogicManager;

        [Inject]
        private IScreenManager _screenManager;

        [SerializeField]
        private Dropdown _firstPlayer;
        [SerializeField]
        private Dropdown _secondPlayer;

        [SerializeField]
        private Button _playButton;

        private void Start()
        {
            _playButton.onClick.AddListener(OnButtonClick);
        }

        private void OnButtonClick()
        {
            AddPlayer(_firstPlayer.value, GameCharacter.X);
            AddPlayer(_secondPlayer.value, GameCharacter.O);
            _screenManager.ShowScreen("Game");
        }

        private void AddPlayer(int value, GameCharacter character)
        {
            switch (value)
            {
                case 0:
                    _gameLogicManager.AddPlayer(PlayerType.UIPlayer);
                    break;
                case 1:
                    _gameLogicManager.AddPlayer(PlayerType.EasyBot);
                    break;
                case 2:
                    _gameLogicManager.AddPlayer(PlayerType.MiddleBot);
                    break;
                case 3:
                    _gameLogicManager.AddPlayer(PlayerType.HardBot);
                    break;
                default:
                    Debug.LogError("Error");
                    break;
            }
        }
    }
}