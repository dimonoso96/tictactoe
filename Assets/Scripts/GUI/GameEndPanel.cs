﻿using PanelManagement;
using PanelManagement.Data;
using PanelManagement.Panel;
using ScreenManagement;
using TicTacToe.Game;
using TicTacToe.Player;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace TicTacToe
{
    public class GameEndPanel : PanelElement
    {
        [SerializeField]
        private Text _text;
        [SerializeField]
        private Button _restartButton;
        [SerializeField]
        private Button _mainMenuButton;

        [SerializeField]
        private string _mainMenuKey;

        [Inject]
        private IGameLogicManager _gameLogicManager;

        [Inject]
        private IScreenManager _screenManager;

        protected override void Awake()
        {
            base.Awake();
            _restartButton.onClick.AddListener(OnButtonRestartClick);
            _mainMenuButton.onClick.AddListener(OnButtonMenuClick);

            _gameLogicManager.OnDrawEvent += OnDraw;
            _gameLogicManager.OnCharacterWinEvent += OnCharacterWin;
        }

        private void OnCharacterWin(GameCharacter character)
        {
            var gameEndPanelData = new GameEndPanelData();
            gameEndPanelData.WinText = "Player " + character.ToString() + " win!";
            PanelManager.ShowPanel(PanelName, gameEndPanelData);
        }

        private void OnDraw()
        {
            var gameEndPanelData = new GameEndPanelData();
            gameEndPanelData.WinText = "Draw!";
            PanelManager.ShowPanel(PanelName, gameEndPanelData);
        }

        public override void Show(AbstractPanelData data)
        {
            var gameEndScreenData = data as GameEndPanelData;
            if (gameEndScreenData != null)
            {
                _text.text = gameEndScreenData.WinText;
                base.Show(data);
            }
        }

        private void OnButtonRestartClick()
        {
            PanelManager.HidePanel(PanelName);
        }

        private void OnButtonMenuClick()
        {
            PanelManager.HidePanel(PanelName);
            _screenManager.ShowScreen(_mainMenuKey);
        }
    }

    public class GameEndPanelData : AbstractPanelData
    {
        public string WinText;
    }
}
