﻿using TicTacToe.Game;
using TicTacToe.GUI.Factory;
using UnityEngine;
using Zenject;

namespace TicTacToe.GUI
{
    public class ButtonsContainer : MonoBehaviour
    {
        [Inject]
        private IGameLogicManager _gameLogicManager;

        [Inject]
        private GBFactory _gameButtonFactory;

        [SerializeField]
        private RectTransform _content;

        private void Start()
        {
            for (int i = 0; i < GameLogicManager.ElementsWidth; i++)
            {
                for (int j = 0; j < GameLogicManager.ElementsWidth; j++)
                {
                    var gameButton = _gameButtonFactory.Create(_content);
                    gameButton.SetPosition(i, j);
                }
            }
        }
    }
}
