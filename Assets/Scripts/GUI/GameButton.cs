﻿using TicTacToe.Game;
using TicTacToe.Player;
using TicTacToe.Player.State;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace TicTacToe.GUI
{
    [RequireComponent(typeof(Button))]
    public class GameButton : MonoBehaviour
    {
        [Inject]
        private IGameLogicManager _gameLogicManager;

        private int _x;
        private int _y;

        Button _button;
        Button button { get { if (_button == null) _button = GetComponent<Button>(); return _button; } }

        Text _text;
        Text text { get { if (_text == null) _text = GetComponentInChildren<Text>(); return _text; } }

        private void Start()
        {
            button.onClick.AddListener(OnButtonClick);
            text.text = "";
        }

        public void SetPosition(int x, int y)
        {
            _x = x;
            _y = y;
            _gameLogicManager.Table[_x, _y].OnValueChange += ChangeState;
        }

        private void ChangeState(GameCharacter character)
        {
            switch (character)
            {
                case GameCharacter.None:
                    text.text = "";
                    break;
                case GameCharacter.X:
                    text.text = "X";
                    break;
                case GameCharacter.O:
                    text.text = "O";
                    break;
            }
        }

        public void OnButtonClick()
        {
            var currentPlayer = _gameLogicManager.CurrentPlayer;
            if (currentPlayer != null && currentPlayer.StateMachine.CurrentState == PlayerState.Turn && currentPlayer is UIPlayer)
            {
                _gameLogicManager.AddCharacter(_x, _y, currentPlayer);
            }
        }
    }
}
