﻿using System.Collections;
using ScreenManagement;
using UnityEngine;
using Zenject;

namespace TicTacToe
{
    public class InitialisingScript : MonoBehaviour
    {
        [Inject]
        private IScreenManager _screenManager;

        private IEnumerator Start()
        {
            yield return null;
            _screenManager.ShowScreen("SelectMenu");
        }
    }
}