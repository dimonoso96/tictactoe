﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TicTacToe.Player
{
    public enum GameCharacter
    {
		None,
		X,
		O,
    }
}
