﻿using StateMachine;
using TicTacToe.Game;
using TicTacToe.Game.RuleChecking;
using TicTacToe.Player.State;
using UnityEngine;
using Zenject;

namespace TicTacToe.Player.Bots
{
    public class EasyBot : IPlayer
    {
        [Inject]
        private IGameLogicManager _gameLogicManager;

        public GameCharacter Character { get; set; }

        private IStateMachine<PlayerState> _playerStateMachine;

        private bool _isTurn = false;
        private float _waitTime;

        private const float WaitBeforeMove = 1f;

        [Inject]
        private RuleHelper _ruleHelper;

        [Inject]
        public IStateMachine<PlayerState> StateMachine
        {
            get
            {
                return _playerStateMachine;
            }
            private set
            {
                if (_playerStateMachine != null)
                {
                    return;
                }
                _playerStateMachine = value;

                _playerStateMachine.States[PlayerState.Turn].OnEnterState += StartTurn;
                _playerStateMachine.States[PlayerState.Turn].OnExecute += Execute;

                _playerStateMachine.Init();
            }
        }

        private void StartTurn()
        {
            _isTurn = true;
            _waitTime = WaitBeforeMove;
        }

        private void Execute()
        {
            if (_isTurn)
            {
                _waitTime -= Time.deltaTime;
                if (_waitTime < 0)
                {
                    _isTurn = false;
                    var cells = _ruleHelper.GetFreeTableCells();
                    var randomInt = Random.Range(0, cells.Count);
                    _gameLogicManager.AddCharacter(cells[randomInt], this);
                }
            }
        }
    }
}