﻿using StateMachine;
using TicTacToe.Player.State;
using Zenject;

namespace TicTacToe.Player
{
    public class UIPlayer : IPlayer
    {
        public GameCharacter Character { get; set; }

        private IStateMachine<PlayerState> _playerStateMachine;
        
        [Inject]
        public IStateMachine<PlayerState> StateMachine
        {
            get
            {
                return _playerStateMachine;
            }
            private set
            {
                if (_playerStateMachine != null)
                {
                    return;
                }
                _playerStateMachine = value;
                _playerStateMachine.Init();
            }
        }

        //public class UIPlayerProduct : Factory<UIPlayer>
        //{
        //}
    }
}
