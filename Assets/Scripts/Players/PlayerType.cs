﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TicTacToe.Player
{
    public enum PlayerType
    {
		UIPlayer,
		EasyBot,
		MiddleBot,
		HardBot,
    }
}
