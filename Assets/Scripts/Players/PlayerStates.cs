﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TicTacToe.Player.State
{
    public enum PlayerState
    {
        WaitOtherPlayer,
        Turn,
    }
}
