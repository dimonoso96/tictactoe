﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StateMachine;
using TicTacToe.Player.State;

namespace TicTacToe.Player
{
    public interface IPlayer
    {
        GameCharacter Character { get; set; }

        IStateMachine<PlayerState> StateMachine{ get; }
    }
}
