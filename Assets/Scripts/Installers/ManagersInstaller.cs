using UnityEngine;
using Zenject;
using TicTacToe.Game;
using System.Collections.Generic;
using ScreenManagement;
using PanelManagement;

namespace TicTacToe.Installers
{
    public class ManagersInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            BindFromComponentInChildren<IGameLogicManager>();
            BindFromComponentInChildren<IScreenManager>();
            BindFromComponentInChildren<IPanelManager>();
        }

        private void BindFromComponentInChildren<T>()
        {
            var manager = GetComponentInChildren<T>();
            Container.Bind<T>().FromInstance(manager).AsSingle().NonLazy();
        }
    }
}