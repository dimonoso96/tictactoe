using System;
using StateMachine;
using StateMachine.ScriptableObjects;
using TicTacToe.Game.State;
using TicTacToe.Game.StateMachine;
using TicTacToe.Player.State;
using TicTacToe.Player.StateMachine;
using UnityEngine;
using Zenject;

namespace TicTacToe.Installers
{
    public class StateMachineInstaller : MonoInstaller
    {
        public string GameStateMachineSettingsPath;
        public string PlayerStateMachineSettingsPath;

        private PlayerStateMachine _playerStateMachineSettings;
        private GameStateMachine _gameStateMachineSettings;

        [Inject]
        private TickableManager _tickableManager;

        public override void InstallBindings()
        {
            //_tickableManager = Container.Resolve<TickableManager>();

            // GameStateMachine
            _gameStateMachineSettings = Resources.Load<GameStateMachine>(GameStateMachineSettingsPath);

            Container.Bind(typeof(IStateMachine<GameStates>), typeof(ITickable)).FromMethod(CreateGameStateMachine).AsSingle().NonLazy();

            // PlayerStateMachine
            _playerStateMachineSettings = Resources.Load<PlayerStateMachine>(PlayerStateMachineSettingsPath);

            Container.Bind<IStateMachine<PlayerState>>().FromMethod(CreatePlayerStateMachine).AsTransient().Lazy();
        }

        public BaseStateMachine<GameStates> CreateGameStateMachine(InjectContext context)
        {
            var stateMachine = CreateBaseStateMachine<GameStates>(_gameStateMachineSettings);
            return stateMachine;
        }

        public IStateMachine<PlayerState> CreatePlayerStateMachine(InjectContext context)
        {
            var stateMachine = CreateBaseStateMachine<PlayerState>(_playerStateMachineSettings);
            _tickableManager.Add(stateMachine);
            return stateMachine;
        }

        private BaseStateMachine<T> CreateBaseStateMachine<T>(StateMachineScriptableObject<T> scriptableObject) where T : struct, IComparable, IFormattable, IConvertible
        {
            var stateMachine = Container.Instantiate<BaseStateMachine<T>>();

            for (int i = 0; i < scriptableObject.States.Length; i++)
            {
                var state = new BaseState<T>();
                stateMachine.AddState(scriptableObject.States[i], state);
            }

            for (int i = 0; i < scriptableObject.fromList.Length && i < scriptableObject.toList.Length; i++)
            {
                stateMachine.AddTrigger(scriptableObject.fromList[i], scriptableObject.toList[i]);
            }

            stateMachine.InitState = scriptableObject.InitialState;

            return stateMachine;
        }
    }
}
