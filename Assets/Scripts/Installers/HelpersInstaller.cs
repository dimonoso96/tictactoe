using UnityEngine;
using Zenject;
using TicTacToe.Game.RuleChecking;

namespace TicTacToe.Game.Helpers.Installer
{
    public class HelpersInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<RuleHelper>().ToSelf().AsSingle().NonLazy();
        }
    }
}