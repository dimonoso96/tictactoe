﻿using UnityEngine;
using Zenject;

namespace TicTacToe.GUI.Factory
{
    public class GameButtonFactory : IFactory<Transform, GameButton>
    {
        [Inject]
        private DiContainer _container;

        [Inject(Id = "ButtonPrefab")]
        private GameObject buttonPrefab;

        public GameButton Create(Transform parentTransform)
        {
            var go = _container.InstantiatePrefab(buttonPrefab, parentTransform);
            return go.GetComponent<GameButton>();
        }
    }

    public class GBFactory : Factory<Transform, GameButton>
    {
    }
}
