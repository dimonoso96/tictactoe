﻿using System.Collections.Generic;
using TicTacToe.Player.Bots;
using UnityEngine;
using Zenject;

namespace TicTacToe.Player.Factory.Product
{
    public class EasyBotProductFactory : MonoBehaviour, IPlayerProductFactory
    {
        [SerializeField]
        private PlayerType _playerProductType;

        public PlayerType PlayerProductType { get { return _playerProductType; } }

        [Inject]
        private DiContainer _diContainer;

        private List<IPlayer> _playerProductPool = new List<IPlayer>();
        private List<IPlayer> _createdPlayers = new List<IPlayer>();

        public IPlayer Create()
        {
            if (_playerProductPool.Count > 0)
            {
                var timeElem = _playerProductPool[0];
                _playerProductPool.RemoveAt(0);
                _createdPlayers.Add(timeElem);
                return timeElem;
            }
            var elem = _diContainer.Instantiate<EasyBot>();
            _createdPlayers.Add(elem);
            return elem;
        }

        public void Clear()
        {
            _playerProductPool.AddRange(_createdPlayers);
            _createdPlayers.Clear();
        }
    }
}
