﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace TicTacToe.Player.Factory.Product
{
    public interface IPlayerProductFactory : IFactory<IPlayer>
    {
        PlayerType PlayerProductType { get; }

        void Clear();
    }
}
