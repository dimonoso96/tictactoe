﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace TicTacToe.Player.Factory
{
    public interface IPlayerFactory : IFactory<PlayerType, IPlayer>
    {
        void Clear();
    }
}
