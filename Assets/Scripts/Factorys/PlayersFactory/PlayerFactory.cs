﻿using System.Collections.Generic;
using TicTacToe.Player.Factory.Product;
using UnityEngine;
using Zenject;

namespace TicTacToe.Player.Factory
{
    public class PlayerFactory : MonoBehaviour, IPlayerFactory
    {
        private Dictionary<PlayerType, IPlayerProductFactory> _playerProductFactorys = new Dictionary<PlayerType, IPlayerProductFactory>();

        [Inject]
        private void AddProducts(List<IPlayerProductFactory> playerProductFactorys)
        {
            for (int i = 0; i < playerProductFactorys.Count; i++)
            {
                _playerProductFactorys.Add(playerProductFactorys[i].PlayerProductType, playerProductFactorys[i]);
            }
        }

        public IPlayer Create(PlayerType playerType)
        {
            IPlayerProductFactory productFactory = _playerProductFactorys[playerType];

            if (productFactory != null)
            {
                return productFactory.Create();
            }
            return null;
        }

        public void Clear()
        {
            foreach (var keyValue in _playerProductFactorys)
            {
                keyValue.Value.Clear();
            }
        }
    }
}
