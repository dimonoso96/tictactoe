using StateMachine;
using TicTacToe.GUI;
using TicTacToe.GUI.Factory;
using TicTacToe.Player.Factory.Product;
using TicTacToe.Player.State;
using UnityEngine;
using Zenject;

namespace TicTacToe.Player.Factory.Installer
{
    public class FactorysInstaller : MonoInstaller
    {
        public GameObject ButtonsPrefab;

        public override void InstallBindings()
        {
            var playerFactory = GetComponentInChildren<IPlayerFactory>();
            Container.Bind<IPlayerFactory>().FromInstance(playerFactory).AsSingle().NonLazy();
            var playerProducts = GetComponentsInChildren<IPlayerProductFactory>();
            for (int i = 0; i < playerProducts.Length; i++)
            {
                Container.Bind<IPlayerProductFactory>().FromInstance(playerProducts[i]);
            }

            Container.Bind<GameObject>().WithId("ButtonPrefab").FromInstance(ButtonsPrefab);
            Container.BindFactory<Transform, GameButton, GBFactory>().FromFactory<GameButtonFactory>();
        }
    }
}
