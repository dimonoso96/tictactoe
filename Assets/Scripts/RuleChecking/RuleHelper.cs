﻿using System;
using System.Collections.Generic;
using UnityEngine;
using TicTacToe.Player;

namespace TicTacToe.Game.RuleChecking
{
    public class RuleHelper
    {
        private TableCell[,] _table;

        private List<RuleChecker> _rules;
        private RuleChecker _drawRule;

        public void CreateRule(TableCell[,] table)
        {
            _rules = new List<RuleChecker>();
            _table = table;
            _drawRule = new RuleChecker();
            var diagonalRule1 = new RuleChecker();
            var diagonalRule2 = new RuleChecker();
            for (int i = 0; i < _table.GetLength(0); i++)
            {
                var rowRule = new RuleChecker();
                var columRule = new RuleChecker();

                for (int j = 0; j < _table.GetLength(1); j++)
                {
                    rowRule.AddCell(_table[i, j]);
                    columRule.AddCell(_table[j, i]);

                    _drawRule.AddCell(_table[i, j]);
                }
                diagonalRule1.AddCell(_table[i, i]);
                diagonalRule2.AddCell(_table[_table.GetLength(0) - 1 - i, i]);

                _rules.Add(rowRule);
                _rules.Add(columRule);
            }

            //_rules.Add(_drawRule);
            _rules.Add(diagonalRule1);
            _rules.Add(diagonalRule2);
        }

        public GameCharacter GetWinCharacter()
        {
            for (int i = 0; i < _rules.Count; i++)
            {
                var winCharacter = _rules[i].GetWinCharacter();
                if(GameCharacter.None != winCharacter)
                {
                    return winCharacter;
                }
            }
            return GameCharacter.None;

        }

        public bool IsDraw()
        {
            return _drawRule.IsDraw();
        }

        public List<TableCell> GetAttackDefenceTableCells(GameCharacter gameCharacter)
        {
            var list = new List<TableCell>();
            for (int i = 0; i < _rules.Count; i++)
            {
                list.AddRange(_rules[i].GetAttackDefenceTableCells(gameCharacter));
            }
            return list;
        }

        public List<TableCell> GetFreeTableCells()
        {
            var list = new List<TableCell>();
            for (int i = 0; i < _rules.Count; i++)
            {
                list.AddRange(_rules[i].GetFreeTableCells());
            }
            return list;
        }
    }
}
