﻿using System;
using System.Collections.Generic;
using System.Linq;
using TicTacToe.Player;

namespace TicTacToe.Game.RuleChecking
{
    public class RuleChecker
    {
        private List<TableCell> _cells = new List<TableCell>();

        public void AddCell(TableCell cell)
        {
            _cells.Add(cell);
        }

        public List<TableCell> GetFreeTableCells()
        {
            return _cells.FindAll(x => x.Character == GameCharacter.None);
        }

        public List<TableCell> GetAttackDefenceTableCells(GameCharacter gameCharacter)
        {
            var res = new List<TableCell>();
            var noneCount = _cells.Count(x => x.Character == GameCharacter.None);
            var attackCount = _cells.Count(x => x.Character == gameCharacter);
            var cellsCount = _cells.Count;
            if (cellsCount == attackCount + noneCount && noneCount == 1)
            {
                res.Add(_cells.First(x => x.Character == GameCharacter.None));
            }
            return res;
        }

        public GameCharacter GetWinCharacter()
        {
            var xCount = _cells.Count(x => x.Character == GameCharacter.X);
            var oCount = _cells.Count(x => x.Character == GameCharacter.O);
            var cellsCount = _cells.Count;
            if (xCount == cellsCount)
            {
                return GameCharacter.X;
            }
            if (oCount == cellsCount)
            {
                return GameCharacter.O;
            }
            return GameCharacter.None;
        }

        public bool IsDraw()
        {
            var noneCount = _cells.Count(x => x.Character == GameCharacter.None);
            return noneCount == 0;
        }
    }
}
